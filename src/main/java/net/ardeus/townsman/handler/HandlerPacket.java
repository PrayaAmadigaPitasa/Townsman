package net.ardeus.townsman.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.townsman.Townsman;

public class HandlerPacket extends Handler {
	
	protected HandlerPacket(Townsman plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerPacket> getAllHandlerPacket() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerPacket> allHandlerPacket = new ArrayList<HandlerPacket>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerPacket) {
				final HandlerPacket handlerPacket = (HandlerPacket) handler;
				
				allHandlerPacket.add(handlerPacket);
			}
		}
		
		return allHandlerPacket;
	}
}
