package net.ardeus.townsman.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.townsman.Townsman;

public class HandlerBridge extends Handler {
	
	protected HandlerBridge(Townsman plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerBridge> getAllHandlerBridge() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerBridge> allHandlerBridge = new ArrayList<HandlerBridge>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerBridge) {
				final HandlerBridge handlerBridge = (HandlerBridge) handler;
				
				allHandlerBridge.add(handlerBridge);
			}
		}
		
		return allHandlerBridge;
	}
}
