package net.ardeus.townsman.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.townsman.Townsman;

public abstract class HandlerMenuExecutor extends Handler {
	
	protected HandlerMenuExecutor(Townsman plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerMenuExecutor> getAllHandlerMenuExecutor() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerMenuExecutor> allHandlerMenuExecutor = new ArrayList<HandlerMenuExecutor>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerMenuExecutor) {
				final HandlerMenuExecutor handlerMenu = (HandlerMenuExecutor) handler;
				
				allHandlerMenuExecutor.add(handlerMenu);
			}
		}
		
		return allHandlerMenuExecutor;
	}
}
