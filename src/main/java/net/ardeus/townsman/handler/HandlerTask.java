package net.ardeus.townsman.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.townsman.Townsman;

public abstract class HandlerTask extends Handler {
	
	protected HandlerTask(Townsman plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerTask> getAllHandlerTask() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerTask> allHandlerTask = new ArrayList<HandlerTask>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerTask) {
				final HandlerTask handlerTask = (HandlerTask) handler;
				
				allHandlerTask.add(handlerTask);
			}
		}
		
		return allHandlerTask;
	}
}
