package net.ardeus.townsman.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.townsman.Townsman;

public abstract class HandlerTabCompleter extends Handler {
	
	protected HandlerTabCompleter(Townsman plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerTabCompleter> getAllHandlerTabCompleter() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerTabCompleter> allHandlerTabCompleter = new ArrayList<HandlerTabCompleter>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerTabCompleter) {
				final HandlerTabCompleter handlerTabCompleter = (HandlerTabCompleter) handler;
				
				allHandlerTabCompleter.add(handlerTabCompleter);
			}
		}
		
		return allHandlerTabCompleter;
	}
}
