package net.ardeus.townsman.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.townsman.Townsman;

public abstract class HandlerCommand extends Handler {
	
	protected HandlerCommand(Townsman plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerCommand> getAllHandlerCommand() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerCommand> allHandlerCommand = new ArrayList<HandlerCommand>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerCommand) {
				final HandlerCommand handlerCommand = (HandlerCommand) handler;
				
				allHandlerCommand.add(handlerCommand);
			}
		}
		
		return allHandlerCommand;
	}
}
