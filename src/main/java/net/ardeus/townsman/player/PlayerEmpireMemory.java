package net.ardeus.townsman.player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.manager.player.PlayerEmpireManager;

public final class PlayerEmpireMemory extends PlayerEmpireManager {

	private final Map<UUID, PlayerEmpire> mapPlayerEmpire = new HashMap<UUID, PlayerEmpire>();
	
	private PlayerEmpireMemory(Townsman plugin) {
		super(plugin);
	}
	
	private static class PlayerEmpireMemorySingleton {
		private static final PlayerEmpireMemory INSTANCE;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			INSTANCE = new PlayerEmpireMemory(plugin);
		}
	}

	public static final PlayerEmpireMemory getInstance() {
		return PlayerEmpireMemorySingleton.INSTANCE;
	}
	
	@Override
	public final PlayerEmpire getPlayerEmpire(UUID playerId) {
		return this.mapPlayerEmpire.get(playerId);
	}
}