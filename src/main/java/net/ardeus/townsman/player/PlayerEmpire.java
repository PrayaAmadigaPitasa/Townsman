package net.ardeus.townsman.player;

import java.util.UUID;

import net.ardeus.townsman.empire.Empire;
import net.ardeus.townsman.empire.EmpireMember;
import net.ardeus.townsman.empire.EmpireMemberRole;
import net.ardeus.townsman.empire.EmpireMemory;

public final class PlayerEmpire {

	private final UUID playerId;
	
	private UUID empireId;
	private EmpireMemberRole role;
	
	protected PlayerEmpire(UUID playerId, UUID empireId, EmpireMemberRole role) {
		if (playerId == null) {
			throw new IllegalArgumentException();
		} else {
			this.playerId = playerId;
			this.empireId = empireId;
			this.role = role;
		}
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final Empire getEmpire() {
		final EmpireMemory empireMemory = EmpireMemory.getInstance();
		
		validate();
		
		return empireMemory.getEmpire(this.empireId);
	}
	
	public final EmpireMemberRole getRole() {
		final Empire empire = getEmpire();
		
		if (empire != null) {
			final EmpireMember empireMember = empire.getEmpireMember();
			
			return empireMember.getMemberRole(getPlayerId());
		} else {
			return null;
		}
	}
	
	public final boolean hasEmpire() {
		return getEmpire() != null;
	}
	
	private final void validate() {
		final EmpireMemory empireMemory = EmpireMemory.getInstance();
		final Empire empire = empireMemory.getEmpire(this.empireId);
		
		if (empire != null) {
			final EmpireMember empireMember = empire.getEmpireMember();
			final EmpireMemberRole empireMemberRole = empireMember.getMemberRole(getPlayerId());
			
			if (empireMemberRole == null) {
				this.empireId = null;
				this.role = null;
			} else if (empireMemberRole != this.role) {
				this.role = empireMemberRole;
			}
		}
	}
}
