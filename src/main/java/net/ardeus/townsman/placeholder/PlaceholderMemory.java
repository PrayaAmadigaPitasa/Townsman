package net.ardeus.townsman.placeholder;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.manager.plugin.PlaceholderManager;

public final class PlaceholderMemory extends PlaceholderManager {
	
	private final PlaceholderConfig placeholderConfig;
	
	private PlaceholderMemory(Townsman plugin) {
		super(plugin);
		
		this.placeholderConfig = new PlaceholderConfig(plugin);
	};
	
	private static class PlaceholderMemorySingleton {
		private static final PlaceholderMemory instance;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			instance = new PlaceholderMemory(plugin);
		}
	}
	
	public static final PlaceholderMemory getInstance() {
		return PlaceholderMemorySingleton.instance;
	}
	
	public final PlaceholderConfig getPlaceholderConfig() {
		return this.placeholderConfig;
	}
	
	@Override
	protected final HashMap<String, String> getMapPlaceholder() {
		return getPlaceholderConfig().mapPlaceholder;
	}
	
	@Override
	public final Collection<String> getPlaceholderIds() {
		return getPlaceholderConfig().mapPlaceholder.keySet();
	}
	
	@Override
	public final Collection<String> getPlaceholders() {
		return getPlaceholderConfig().mapPlaceholder.values();
	}
	
	@Override
	public final String getPlaceholder(String id) {
		for (String key : getPlaceholderIds()) {
			if (key.equalsIgnoreCase(id)) {
				return getPlaceholderConfig().mapPlaceholder.get(id);
			}
		}
		
		return null;
	}
}
	