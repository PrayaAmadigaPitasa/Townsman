package net.ardeus.townsman.town;

import java.util.UUID;

public final class Town {

	private final UUID townId;
	
	private UUID empireId;
	
	protected Town(UUID townId, UUID empireId) {
		if (townId == null) {
			throw new IllegalArgumentException();
		} else {
			this.townId = townId;
			this.empireId = empireId;
		}
	}
	
	public final UUID getTownId() {
		return this.townId;
	}
	
	public final UUID getEmpireId() {
		return this.empireId;
	}
	
	public final boolean isOwned() {
		return getEmpireId() != null;
	}
}