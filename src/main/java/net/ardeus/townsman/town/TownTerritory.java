package net.ardeus.townsman.town;

import java.util.UUID;

import net.ardeus.townsman.chunk.ChunkLocation;

public final class TownTerritory {

	private final ChunkLocation chunkLocation;
	
	private UUID townId;
	
	protected TownTerritory(ChunkLocation chunkLocation, UUID townId) {
		if (chunkLocation == null) {
			throw new IllegalArgumentException();
		} else {
			this.chunkLocation = chunkLocation;
			this.townId = townId;
		}
	}
	
	public final ChunkLocation getChunkLocation() {
		return this.chunkLocation;
	}
	
	public final UUID getTownId() {
		return this.townId;
	}
	
	public final boolean isTownTerritory() {
		return getTownId() != null;
	}
}
