package net.ardeus.townsman;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.manager.player.PlayerEmpireManager;
import net.ardeus.townsman.manager.player.PlayerManager;
import net.ardeus.townsman.player.PlayerEmpireMemory;

public final class TownsmanPlayerMemory extends PlayerManager {
	
	private final PlayerEmpireManager playerEmpireManager;
	
	protected TownsmanPlayerMemory(Townsman plugin) {
		super(plugin);
		
		this.playerEmpireManager = PlayerEmpireMemory.getInstance();
	}
	
	@Override
	public final PlayerEmpireManager getPlayerEmpireManager() {
		return this.playerEmpireManager;
	}
}