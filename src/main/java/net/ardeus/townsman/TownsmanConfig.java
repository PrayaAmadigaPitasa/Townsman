package net.ardeus.townsman;

import java.io.File;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.praya.agarthalib.database.DatabaseType;
import com.praya.agarthalib.utility.FileUtil;

import net.ardeus.townsman.handler.HandlerConfig;

public final class TownsmanConfig extends HandlerConfig {
	
	private static final String PATH_FILE = "Configuration/config.yml";
	
	public static final FileConfiguration config = new YamlConfiguration();
	
	protected TownsmanConfig(Townsman plugin) {
		super(plugin);
		
		setup();
	}
	
	public final String getGeneralVersion() {
		return config.getString("Configuration.General.Version");
	}
	
	public final String getGeneralLocale() {
		return config.getString("Configuration.General.Locale");
	}
	
	public final boolean isMetricsMessage() {
		return config.getBoolean("Configuration.Metrics.Message");
	}
	
	public final boolean isHookMessage() {
		return config.getBoolean("Configuration.Hook.Message");
	}
	
	public final String getUtilityTooltip() {
		return config.getString("Configuration.Utility.Tooltip");
	}
	
	public final String getUtilityCurrency() {
		return config.getString("Configuration.Utility.Currency");
	}
	
	public final double getEffectRange() {
		return config.getDouble("Configuration.Effect.Range");
	}
	
	public final int getListContent() {
		return config.getInt("Configuration.List.Content");
	}
	
	public final int getPriorityActionbar() {
		return config.getInt("Configuration.Priority.Actionbar");
	}
	
	public final int getPriorityBossBar() {
		return config.getInt("Configuration.Priority.BossBar");
	}
	
	public final DatabaseType getDatabaseServerType() {
		return (DatabaseType) config.get("Configuration.Database.Server_Type");
	}
	
	public final String getDatabaseServerIdentifier() {
		return config.getString("Configuration.Database.Server_Identifier");
	}
	
	public final int getDatabaseLoadTimeout() {
		return config.getInt("Configuration.Database.Load_Timeout");
	}
	
	public final int getDatabasePeriodSave() {
		return config.getInt("Configuration.Database.Periode_Save");
	}
	
	public final String getDatabaseClientHost() {
		return config.getString("Configuration.Database.Client_Host");
	}
	
	public final int getDatabaseClientPort() {
		return config.getInt("Configuration.Database.Client_Port");
	}
	
	public final String getDatabaseClientDatabase() {
		return config.getString("Configuration.Database.Client_Database");
	}
	
	public final String getDatabaseClientUsername() {
		return config.getString("Configuration.Database.Client_Username");
	}
	
	public final String getDatabaseClientPassword() {
		return config.getString("Configuration.Database.Client_Password");
	}
	
	public final void setup() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration configurationResource = FileUtil.getFileConfigurationResource(plugin, PATH_FILE);
		final FileConfiguration configurationFile = FileUtil.getFileConfiguration(file);
		
		loadConfig(config, configurationResource);
		loadConfig(config, configurationFile);
	}
	
	private final void loadConfig(FileConfiguration config, FileConfiguration source) {
		for (String key : source.getKeys(false)) {
			if (key.equalsIgnoreCase("Configuration") || key.equalsIgnoreCase("Config")) {
				final ConfigurationSection dataSection = source.getConfigurationSection(key);
				
				for (String data : dataSection.getKeys(false)) {
					if (data.equalsIgnoreCase("General")) {
						final ConfigurationSection generalDataSection = dataSection.getConfigurationSection(data);
						
						for (String generalData : generalDataSection.getKeys(false)) {
							if (generalData.equalsIgnoreCase("Version")) {
								final String path = "Configuration.General.Version";
								final String generalVersion = generalDataSection.getString(generalData);
								
								config.set(path, generalVersion);
							} else if (generalData.equalsIgnoreCase("Locale")) {
								final String path = "Configuration.General.Locale";
								final String generalLocale = generalDataSection.getString(generalData);
								
								config.set(path, generalLocale);
							}
						}
					} else if (data.equalsIgnoreCase("Metrics")) {
						final ConfigurationSection metricsDataSection = dataSection.getConfigurationSection(data);
						
						for (String metricsData : metricsDataSection.getKeys(false)) {
							if (metricsData.equalsIgnoreCase("Message")) {
								final String path = "Configuration.Metrics.Message";
								final boolean metricsMessage = metricsDataSection.getBoolean(metricsData);
								
								config.set(path, metricsMessage);
							}
						}
					} else if (data.equalsIgnoreCase("Hook")) {
						final ConfigurationSection hookDataSection = dataSection.getConfigurationSection(data);
						
						for (String hookData : hookDataSection.getKeys(false)) {
							if (hookData.equalsIgnoreCase("Message")) {
								final String path = "Configuration.Hook.Message";
								final boolean hookMessage = hookDataSection.getBoolean(hookData);
								
								config.set(path, hookMessage);
							}
						}
					} else if (data.equalsIgnoreCase("Utility")) {
						final ConfigurationSection utilityDataSection = dataSection.getConfigurationSection(data);
						
						for (String utilityData : utilityDataSection.getKeys(false)) {
							if (utilityData.equalsIgnoreCase("Tooltip")) {
								final String path = "Configuration.Utility.Tooltip";
								final String utilityTooltip = utilityDataSection.getString(utilityData);
								
								config.set(path, utilityTooltip);
							} else if (utilityData.equalsIgnoreCase("Currency")) {
								final String path = "Configuration.Utility.Currency";
								final String utilityCurrency = utilityDataSection.getString(utilityData);
								
								config.set(path, utilityCurrency);
							}
						}
					} else if (data.equalsIgnoreCase("Effect")) {
						final ConfigurationSection effectDataSection = dataSection.getConfigurationSection(data);
						
						for (String effectData : effectDataSection.getKeys(false)) {
							if (effectData.equalsIgnoreCase("Range")) {
								final String path = "Configuration.Effect.Range";
								final double effectRange = effectDataSection.getDouble(effectData);
								
								config.set(path, effectRange);
							}
						}
					} else if (data.equalsIgnoreCase("List")) {
						final ConfigurationSection listDataSection = dataSection.getConfigurationSection(data);
						
						for (String listData : listDataSection.getKeys(false)) {
							if (listData.equalsIgnoreCase("Content")) {
								final String path = "Configuration.List.Content";
								final int listContent = listDataSection.getInt(listData);
								
								config.set(path, listContent);
							}
						}
					} else if (data.equalsIgnoreCase("Priority")) {
						final ConfigurationSection priorityDataSection = dataSection.getConfigurationSection(data);
						
						for (String priorityData : priorityDataSection.getKeys(false)) {
							if (priorityData.equalsIgnoreCase("Actionbar")) {
								final String path = "Configuration.Priority.Actionbar";
								final int priorityActionbar = priorityDataSection.getInt(priorityData);
								
								config.set(path, priorityActionbar);
							} else if (priorityData.equalsIgnoreCase("BossBar")) {
								final String path = "Configuration.Priority.BossBar";
								final int priorityBossBar = priorityDataSection.getInt(priorityData);
								
								config.set(path, priorityBossBar);
							}
						}
					} else if (data.equalsIgnoreCase("Database")) {
						final ConfigurationSection databaseDataSection = dataSection.getConfigurationSection(data);
						
						for (String databaseData : databaseDataSection.getKeys(false)) {
							if (databaseData.equalsIgnoreCase("Server_Type")) {
								final String path = "Configuration.Database.Server_Type";
								final String databaseServerTypeText = databaseDataSection.getString(databaseData);
								final DatabaseType databaseServerType = DatabaseType.getDatabaseType(databaseServerTypeText);
								
								config.set(path, databaseServerType);
							} else if (databaseData.equalsIgnoreCase("Server_Identifier")) {
								final String path = "Configuration.Database.Server_Identifier";
								final String databaseServerIdentifier = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseServerIdentifier);
							} else if (databaseData.equalsIgnoreCase("Load_Timeout")) {
								final String path = "Configuration.Database.Load_Timeout";
								final int databaseLoadTimeout = databaseDataSection.getInt(databaseData);
								
								config.set(path, databaseLoadTimeout);
							} else if (databaseData.equalsIgnoreCase("Periode_Save")) {
								final String path = "Configuration.Database.Periode_Save";
								final int databasePeriodSave = databaseDataSection.getInt(databaseData);
								
								config.set(path, databasePeriodSave);
							} else if (databaseData.equalsIgnoreCase("Client_Host")) {
								final String path = "Configuration.Database.Client_Host";
								final String databaseClientHost = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientHost);
							} else if (databaseData.equalsIgnoreCase("Client_Port")) {
								final String path = "Configuration.Database.Client_Port";
								final int databaseClientPort = databaseDataSection.getInt(databaseData);
								
								config.set(path, databaseClientPort);
							} else if (databaseData.equalsIgnoreCase("Client_Database")) {
								final String path = "Configuration.Database.Client_Database";
								final String databaseClientDatabase = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientDatabase);
							} else if (databaseData.equalsIgnoreCase("Client_Username")) {
								final String path = "Configuration.Database.Client_Username";
								final String databaseClientUsername = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientUsername);
							} else if (databaseData.equalsIgnoreCase("Client_Password")) {
								final String path = "Configuration.Database.Client_Password";
								final String databaseClientPassword = databaseDataSection.getString(databaseData);
								
								config.set(path, databaseClientPassword);
							}
						}
					}
				}
			}
		}
	}
}