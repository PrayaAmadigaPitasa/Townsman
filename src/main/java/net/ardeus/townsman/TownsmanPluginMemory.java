package net.ardeus.townsman;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.CommandMemory;
import net.ardeus.townsman.language.LanguageMemory;
import net.ardeus.townsman.manager.plugin.CommandManager;
import net.ardeus.townsman.manager.plugin.LanguageManager;
import net.ardeus.townsman.manager.plugin.MetricsManager;
import net.ardeus.townsman.manager.plugin.PlaceholderManager;
import net.ardeus.townsman.manager.plugin.PluginManager;
import net.ardeus.townsman.manager.plugin.PluginPropertiesManager;
import net.ardeus.townsman.metrics.MetricsMemory;
import net.ardeus.townsman.placeholder.PlaceholderMemory;
import net.ardeus.townsman.plugin.PluginPropertiesMemory;

public final class TownsmanPluginMemory extends PluginManager {

	private final CommandManager commandManager;
	private final LanguageManager languageManager;
	private final PlaceholderManager placeholderManager;
	private final PluginPropertiesManager pluginPropertiesManager;
	private final MetricsManager metricsManager;
	
	protected TownsmanPluginMemory(Townsman plugin) {
		super(plugin);
		
		this.commandManager = CommandMemory.getInstance();
		this.placeholderManager = PlaceholderMemory.getInstance();
		this.languageManager = LanguageMemory.getInstance();
		this.pluginPropertiesManager = PluginPropertiesMemory.getInstance();
		this.metricsManager = MetricsMemory.getInstance();
	}
	
	private static class DreamFishPluginMemorySingleton {
		private static final TownsmanPluginMemory instance;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			instance = new TownsmanPluginMemory(plugin);
		}
	}
	
	protected static final TownsmanPluginMemory getInstance() {
		return DreamFishPluginMemorySingleton.instance;
	}
	
	public final LanguageManager getLanguageManager() {
		return this.languageManager;
	}
	
	public final PlaceholderManager getPlaceholderManager() {
		return this.placeholderManager;
	}
	
	public final PluginPropertiesManager getPluginPropertiesManager() {
		return this.pluginPropertiesManager;
	}
	
	public final MetricsManager getMetricsManager() {
		return this.metricsManager;
	}
	
	public final CommandManager getCommandManager() {
		return this.commandManager;
	}
}
