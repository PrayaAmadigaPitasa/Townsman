package net.ardeus.townsman.chunk;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

import com.praya.agarthalib.utility.WorldUtil;

public class ChunkLocation {

	private final String worldName;
	private final int x;
	private final int z;
	
	public ChunkLocation(Location location) {
		this(location.getChunk());
	}
	
	public ChunkLocation(Block block) {
		this(block.getChunk());
	}
	
	public ChunkLocation(Chunk chunk) {
		this(chunk.getWorld(), chunk.getX(), chunk.getZ());
	}
	
	public ChunkLocation(World world, int x, int z) {
		this(world.getName(), x, z);
	}
	
	protected ChunkLocation(String worldName, int x, int z) {
		if (worldName == null) {
			throw new IllegalArgumentException();
		} else {
			this.worldName = worldName;
			this.x = x;
			this.z = z;
		}
	}
	
	public final World getWorld() {
		return WorldUtil.getWorld(this.worldName);
	}
	
	public final int getX() {
		return this.x;
	}
	
	public final int getZ() {
		return this.z;
	}
	
	public final Chunk getChunk() {
		final World world = getWorld();
		
		return world != null ? world.getChunkAt(getX(), getZ()) : null;
	}
	
	@Override
	public boolean equals(Object object) {
		if (object != null && object instanceof ChunkLocation) {
			final ChunkLocation chunkLocation = (ChunkLocation) object;
			final String worldName = chunkLocation.worldName;
			final int x = chunkLocation.x;
			final int z = chunkLocation.z;
			
			return worldName.equals(this.worldName) && x == this.x && z == this.z;
		} else {
			return false;
		}
	}
}