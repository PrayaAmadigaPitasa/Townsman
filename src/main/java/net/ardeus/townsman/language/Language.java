package net.ardeus.townsman.language;

import java.util.HashMap;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.praya.agarthalib.utility.PlayerUtil;

import core.praya.agarthalib.builder.main.LanguageBuild;
import core.praya.agarthalib.builder.message.MessageBuild;

public enum Language {

	PLUGIN_DEACTIVATED,
	PLUGIN_INFORMATION_NOT_MATCH,
	PLUGIN_LACK_DEPENDENCY,
	PLUGIN_NOT_INSTALLED,
	CONSOLE_COMMAND_FORBIDEN,
	CHARACTER_SPECIAL,
	COMMAND_DISABLED,
	COMMAND_TOWNSMAN_RELOAD_SUCCESS,
	ARGUMENT_NOT_COMPLETE,
	ARGUMENT_DATABASE_PLAYER_NOT_REGISTERED,
	ARGUMENT_ITEM_NOT_EXISTS,
	ARGUMENT_HAS_JOIN_EMPIRE,
	ARGUMENT_NOT_JOIN_EMPIRE,
	ARGUMENT_NOT_LEADER,
	ARGUMENT_INVALID_VALUE,
	ARGUMENT_INVALID_COMMAND,
	ARGUMENT_LACK_MONEY,
	ARGUMENT_LACK_ITEM,
	ARGUMENT_FORMAT_USAGE,
	ARGUMENT_TOWNSMAN_HELP,
	ARGUMENT_TOWNSMAN_ABOUT,
	ARGUMENT_TOWNSMAN_RELOAD,
	ARGUMENT_EMPIRE_CREATE,
	ARGUMENT_EMPIRE_DISBAND,
	TOOLTIP_TOWNSMAN_HELP,
	TOOLTIP_TOWNSMAN_ABOUT,
	TOOLTIP_TOWNSMAN_RELOAD,
	TOOLTIP_EMPIRE_CREATE,
	TOOLTIP_EMPIRE_DISBAND,
	EMPIRE_CREATE_SUCCESS,
	EMPIRE_CREATE_FAILED,
	EMPIRE_DISBAND_SUCCESS,
	EMPIRE_DISBAND_FAILED,
	HELP_HEADER,
	HELP_PAGE,
	HELP_PREVIOUS_PAGE,
	HELP_NEXT_PAGE,
	LIST_HEADER,
	LIST_ITEM,
	PERMISSION_LACK,
	PLAYER_TARGET_OFFLINE,
	PLAYER_NOT_EXISTS,
	FILE_NOT_EXISTS,
	DATABASE_LOAD_TIMEOUT,
	DATABASE_LOAD_FINISHED,
	ITEM_MAINHAND_EMPTY,
	ITEM_NOT_EXIST,
	ITEM_DATABASE_EMPTY;
	
	public final LanguageBuild getLanguage() {
		return getLanguage(null);
	}
	
	public final LanguageBuild getLanguage(String id) {
		final LanguageMemory languageMemory = LanguageMemory.getInstance();
		final LanguageBuild language = languageMemory.getLanguage(id);
		
		return language;
	}
	
	public final MessageBuild getMessage() {
		return getMessage(null);
	}
	
	public final MessageBuild getMessage(CommandSender sender) {
		final LanguageMemory languageMemory = LanguageMemory.getInstance();
		final MessageBuild message = languageMemory.getMessage(sender, this.toString());
		
		return message;
	}
	
	public final String getText() {
		return getText(null);
	}
	
	public final String getText(CommandSender sender) {
		final LanguageMemory languageMemory = LanguageMemory.getInstance();
		final String text = languageMemory.getText(sender, this.toString());
		
		return text;
	}
	
	public final List<String> getListText() {
		return getListText(null);
	}
	
	public final List<String> getListText(CommandSender sender) {
		final LanguageMemory languageMemory = LanguageMemory.getInstance();
		final List<String> listText = languageMemory.getListText(sender, this.toString());
		
		return listText;
	}
	
	public final void broadcast() {
		broadcast(null);
	}
	
	public final void broadcast(String placeholder, String replacement) { 
		broadcast(placeholder, replacement, true);
	}
	
	public final void broadcast(String placeholder, String replacement, boolean playerOnly) { 
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		if (placeholder != null && replacement != null) {
			mapPlaceholder.put(placeholder, replacement);
		}
		
		broadcast(mapPlaceholder, playerOnly);
	}
	
	public final void broadcast(HashMap<String, String> mapPlaceholder) {
		broadcast(mapPlaceholder, true);
	}
	
	public final void broadcast(HashMap<String, String> mapPlaceholder, boolean playerOnly) {
		if (!playerOnly) {
			final MessageBuild message = getMessage();
		
			message.sendMessage();
		}
		
		for (Player player : PlayerUtil.getOnlinePlayers()) {
			final MessageBuild messagePlayer = getMessage(player);
			
			messagePlayer.sendMessage(player, mapPlaceholder);
		}
	}
}
