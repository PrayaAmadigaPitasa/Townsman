package net.ardeus.townsman.metrics;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.manager.plugin.MetricsManager;
import net.ardeus.townsman.metrics.service.BStats;

public class MetricsMemory extends MetricsManager {

	private BStats metricsBStats;
	
	private MetricsMemory(Townsman plugin) {
		super(plugin);
		
		this.metricsBStats = new BStats(plugin);
	}
	
	private static class MetricsMemorySingleton {
		private static final MetricsMemory instance;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			instance = new MetricsMemory(plugin);
		}
	}
	
	public static final MetricsMemory getInstance() {
		return MetricsMemorySingleton.instance;
	}
	
	@Override
	public final BStats getMetricsBStats() {
		return this.metricsBStats;
	}
}
