package net.ardeus.townsman;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.face.Agartha;
import net.ardeus.townsman.manager.game.GameManager;
import net.ardeus.townsman.manager.player.PlayerManager;
import net.ardeus.townsman.manager.plugin.PluginManager;
import net.ardeus.townsman.manager.task.TaskManager;

import com.praya.agarthalib.utility.PlayerUtil;

public class Townsman extends JavaPlugin implements Agartha {
	
	private final String type = "Premium";
	private final String placeholder = "townsman";
	
	private TownsmanConfig mainConfig;
	
	private PluginManager pluginManager;
	private PlayerManager playerManager;
	private GameManager gameManager;
	private TaskManager taskManager;
	
	@Override
	public String getPluginName() {
		return this.getName();
	}

	@Override
	public String getPluginType() {
		return this.type;
	}
	
	@Override
	public String getPluginVersion() {
		return getDescription().getVersion();
	}
	
	@Override
	public String getPluginPlaceholder() {
		return this.placeholder;
	}

	@Override
	public String getPluginWebsite() {
		return getPluginManager().getPluginPropertiesManager().getPluginWebsite();
	}

	@Override
	public String getPluginLatest() {
		return getPluginManager().getPluginPropertiesManager().getPluginVersion();
	}
	
	@Override
	public List<String> getPluginDevelopers() {
		return getPluginManager().getPluginPropertiesManager().getPluginDevelopers();
	}
	
	public final TownsmanConfig getMainConfig() {
		return this.mainConfig;
	}
	
	public final PluginManager getPluginManager() {
		return this.pluginManager;
	}
	
	public final PlayerManager getPlayerManager() {
		return this.playerManager;
	}
	
	public final GameManager getGameManager() {
		return this.gameManager;
	}
	
	public final TaskManager getTaskManager() {
		return this.taskManager;
	}
	
	@Override
	public void onEnable() {
		registerConfig();
		registerManager();
		registerListener();
		registerPlaceholder();
	}
	
	private final void registerConfig() {
		this.mainConfig = new TownsmanConfig(this);
	}
	
	private final void registerManager() {
		this.pluginManager = new TownsmanPluginMemory(this);
		this.playerManager = new TownsmanPlayerMemory(this);
		this.gameManager = new TownsmanGameMemory(this);
		this.taskManager = new TownsmanTaskMemory(this);
	}
	
	private final void registerPlaceholder() {
		getPluginManager().getPlaceholderManager().registerAll();
	}
	
	private final void registerListener() {
		
	}
	
	@Override
	public final void onDisable() {
		final String informationID = "Herbalism Information";
		final BukkitScheduler scheduler = Bukkit.getScheduler();
		
		scheduler.cancelTasks(this);
		
		for (Player player : PlayerUtil.getOnlinePlayers()) {
			Bridge.getBridgeMessage().removeBossBar(player, informationID);
		}
	}
}
