package net.ardeus.townsman.empire;

public final class EmpireBank {

	private double balance;
	
	public EmpireBank() {
		this(null);
	}
	
	protected EmpireBank(Double balance) {
		this.balance = balance != null ? balance : 0;
	}
	
	public final double getBalance() {
		return this.balance;
	}
	
	public final void setBalance(double balance) {
		this.balance = balance;
	}
	
	public final void addBalance(double balance) {
		this.balance += balance;
	}
	
	public final void takeBalance(double balance) {
		this.balance -= balance;
	}
}