package net.ardeus.townsman.empire;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.manager.game.EmpireManager;

public final class EmpireMemory extends EmpireManager {

	private final Map<UUID, Empire> mapEmpire = new HashMap<UUID, Empire>();
	
	private EmpireMemory(Townsman plugin) {
		super(plugin);
	}
	
	private static class EmpireMemorySingleton {
		private static final EmpireMemory INSTANCE;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			INSTANCE = new EmpireMemory(plugin);
		}
	}
	
	public static final EmpireMemory getInstance() {
		return EmpireMemorySingleton.INSTANCE;
	}

	@Override
	public final Collection<UUID> getEmpireIds() {
		return getEmpireIds(true);
	}
	
	protected final Collection<UUID> getEmpireIds(boolean clone) {
		final Collection<UUID> empireIds = this.mapEmpire.keySet();
		
		return clone ? new ArrayList<UUID>(empireIds) : empireIds;
	}

	@Override
	public final Collection<Empire> getAllEmpire() {
		return getAllEmpire(true);
	}
	
	protected final Collection<Empire> getAllEmpire(boolean clone) {
		final Collection<Empire> allEmpire = this.mapEmpire.values();
		
		return clone ? new ArrayList<Empire>(allEmpire) : allEmpire;
	}

	@Override
	public final Empire getEmpire(UUID empireId) {
		return this.mapEmpire.get(empireId);
	}

	@Override
	public final Empire getEmpire(String empireName) {
		if (empireName != null) {
			for (Empire key : getAllEmpire(false)) {
				if (key.getName().equalsIgnoreCase(empireName)) {
					return key;
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final Empire getPlayerEmpire(UUID playerId) {
		if (playerId != null) {
			for (Empire key : getAllEmpire(false)) {
				final EmpireMember empireMember = key.getEmpireMember();
				
				if (empireMember.isMember(playerId)) {
					return key;
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(Empire empire) {
		if (empire != null) {
			final EmpireMember empireMember = empire.getEmpireMember();
			final UUID leaderId = empireMember.getLeaderId();
			final UUID empireId = empire.getId();
			
			if (!isPlayerJoinEmpire(leaderId) && !this.mapEmpire.containsKey(empireId)) {
				
				this.mapEmpire.put(empireId, empire);
				
				return true;
			}
		}
		
		return false;
	}
	
	protected final boolean unregister(Empire empire) {
		if (empire != null && this.mapEmpire.containsValue(empire)) {
			final UUID empireId = empire.getId();
			
			this.mapEmpire.remove(empireId);
			
			return true;
		} else {
			return false;
		}
	}
}
