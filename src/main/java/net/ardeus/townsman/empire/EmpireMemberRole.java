package net.ardeus.townsman.empire;

public enum EmpireMemberRole {

	LEADER,
	DEPUTY,
	OFFICER,
	ELDER,
	COMMONER;
}
