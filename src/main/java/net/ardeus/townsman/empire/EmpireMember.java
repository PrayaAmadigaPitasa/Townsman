package net.ardeus.townsman.empire;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

public final class EmpireMember {

	protected UUID leaderId;
	protected UUID deputyId;
	
	protected final Set<UUID> officerIds;
	protected final Set<UUID> elderIds;
	protected final Set<UUID> commonerIds;
	
	public EmpireMember(OfflinePlayer player) {
		this(player.getUniqueId());
	}
	
	protected EmpireMember(UUID leaderId) {
		this(leaderId, null);
	}
	
	protected EmpireMember(UUID leaderId, UUID deputyId) {
		this(leaderId, deputyId, null, null, null);
	}
	
	protected EmpireMember(UUID leaderId, UUID deputyId, Set<UUID> officerIds, Set<UUID> elderIds, Set<UUID> commonerIds) {
		if (leaderId == null) {
			throw new IllegalArgumentException();
		} else {
			this.leaderId = leaderId;
			this.deputyId = deputyId;
			this.officerIds = officerIds != null ? officerIds : new HashSet<UUID>();
			this.elderIds = officerIds != null ? elderIds : new HashSet<UUID>();
			this.commonerIds = officerIds != null ? commonerIds : new HashSet<UUID>();
		}
	}
	
	public final UUID getLeaderId() {
		return this.leaderId;
	}
	
	public final UUID getDeputyId() {
		return this.deputyId;
	}
	
	public final Set<UUID> getOfficerIds() {
		return getOfficerIds(true);
	}
	
	protected final Set<UUID> getOfficerIds(boolean clone) {
		final Set<UUID> officerIds = this.officerIds;
		
		return clone ? new HashSet<UUID>(officerIds) : officerIds;
	}
	
	public final Set<UUID> getElderIds() {
		return getElderIds(true);
	}
	
	protected final Set<UUID> getElderIds(boolean clone) {
		final Set<UUID> elderIds = this.elderIds;
		
		return clone ? new HashSet<UUID>(elderIds) : elderIds;
	}
	
	public final Set<UUID> getCommonerIds() {
		return getCommonerIds(true);
	}
	
	protected final Set<UUID> getCommonerIds(boolean clone) {
		final Set<UUID> commonerIds = this.commonerIds;
		
		return clone ? new HashSet<UUID>(commonerIds) : commonerIds;
	}
	
	public final Set<UUID> getAllMemberIds() {
		final Set<UUID> allMemberIds = new HashSet<>();
		
		allMemberIds.add(getLeaderId());
		allMemberIds.add(getDeputyId());
		allMemberIds.addAll(getOfficerIds(false));
		allMemberIds.addAll(getElderIds(false));
		allMemberIds.addAll(getCommonerIds(false));
		
		return allMemberIds; 
	}
	
	public final Set<UUID> getRoleMemberIds(EmpireMemberRole role) {
		final Set<UUID> roleMemberIds = new HashSet<>();
		
		if (role != null) {
			switch (role) {
			case LEADER: 
				roleMemberIds.add(getLeaderId()); 
				break;
			case DEPUTY:
				roleMemberIds.add(getDeputyId());
				break;
			case OFFICER:
				roleMemberIds.addAll(getOfficerIds(false));
				break;
			case ELDER:
				roleMemberIds.addAll(getElderIds(false));
				break;
			case COMMONER:
				roleMemberIds.addAll(getCommonerIds(false));
				break;
			}
		}
		
		return roleMemberIds;
	}
	
	public final EmpireMemberRole getMemberRole(OfflinePlayer player) {
		return player != null ? getMemberRole(player.getUniqueId()) : null;
	}
	
	public final EmpireMemberRole getMemberRole(UUID playerId) {
		if (playerId != null) {
			if (getLeaderId().equals(playerId)) {
				return EmpireMemberRole.LEADER;
			} else if (getDeputyId().equals(playerId)) {
				return EmpireMemberRole.DEPUTY;
			} else if (getOfficerIds(false).contains(playerId)) {
				return EmpireMemberRole.OFFICER;
			} else if (getElderIds(false).contains(playerId)) {
				return EmpireMemberRole.ELDER;
			} else if (getCommonerIds(false).contains(playerId)) {
				return EmpireMemberRole.COMMONER;
			}
		}
		
		return null;
	}
	
	public final boolean isMember(OfflinePlayer player) {
		return getMemberRole(player) != null;
	}
	
	public final boolean isMember(UUID playerId) {
		return getMemberRole(playerId) != null;
	}
	
	protected final void kickMember(UUID playerId) {
		if (playerId != null) {
			if (getLeaderId().equals(playerId)) {
				this.leaderId = this.deputyId;
				this.deputyId = null;
			} else if (getDeputyId().equals(playerId)) {
				this.deputyId = null;
			} else if (getOfficerIds(false).contains(playerId)) {
				this.officerIds.remove(playerId);
			} else if (getElderIds(false).contains(playerId)) {
				this.elderIds.remove(playerId);
			} else if (getCommonerIds(false).contains(playerId)) {
				this.commonerIds.remove(playerId);
			}
		}
	}
}
