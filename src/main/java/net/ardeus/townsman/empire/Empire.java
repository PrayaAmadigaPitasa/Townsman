package net.ardeus.townsman.empire;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.praya.agarthalib.utility.PlayerUtil;

import net.ardeus.townsman.language.Language;
import net.ardeus.townsman.player.PlayerEmpire;
import net.ardeus.townsman.player.PlayerEmpireMemory;

public final class Empire {

	private final UUID id;
	private final EmpireMember empireMember;
	private final EmpireBank empireBank;

	private String name;
	
	public Empire(String name, OfflinePlayer player) {
		if (name == null || player == null) {
			throw new IllegalArgumentException();
		} else {
			final EmpireMember empireMember = new EmpireMember(player);
			
			this.id = UUID.randomUUID();
			this.name = name;
			this.empireMember = empireMember;
			this.empireBank = new EmpireBank();
		}
	}
	
	protected Empire(UUID id, String name, EmpireMember empireMember, EmpireBank empireBank) {
		if (id == null || name == null || empireMember == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.name = name;
			this.empireMember = empireMember;
			this.empireBank = empireBank != null ? empireBank : new EmpireBank();
		}
	}
	
	public final UUID getId() {
		return this.id;
	}
	
	public final String getName() {
		return this.name;
	}
	
	public final boolean setName(String name) {
		final EmpireMemory empireMemory = EmpireMemory.getInstance();
		
		if (name != null && !empireMemory.isExists(name)) {
			
			this.name = name;
			
			return true;
		} else {
			return false;
		}
	}
	
	public final EmpireMember getEmpireMember() {
		return this.empireMember;
	}
	
	public final EmpireBank getEmpireBank() {
		return this.empireBank;
	}
	
	public final boolean register() {
		return register(true);
	}
	
	public final boolean register(boolean broadcast) {
		final EmpireMemory empireMemory = EmpireMemory.getInstance();
		final boolean register = empireMemory.register(this);
		
		if (register) {
			if (broadcast) {
				final EmpireMember empireMember = getEmpireMember();
				final UUID leaderId = empireMember.getLeaderId();
				final OfflinePlayer leader = PlayerUtil.getPlayer(leaderId);
				final String leaderName = leader != null ? leader.getName() : "Unknown";
				final String empireName = getName();
				final Language language = Language.EMPIRE_CREATE_SUCCESS;
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("player", leaderName);
				mapPlaceholder.put("empire", empireName);
				
				language.broadcast(mapPlaceholder);
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	public final boolean disband() {
		return disband(true);
	}
	
	public final boolean disband(boolean broadcast) {
		final EmpireMemory empireMemory = EmpireMemory.getInstance();
		final boolean unregister = empireMemory.unregister(this);
		
		if (unregister) {
			if (broadcast) {
				final Language language = Language.EMPIRE_DISBAND_SUCCESS;
				final String empireName = getName();
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("empire", empireName);
				
				language.broadcast(mapPlaceholder);
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	public final void synchronize() {
		final PlayerEmpireMemory playerEmpireMemory = PlayerEmpireMemory.getInstance();
		final EmpireMember empireMember = getEmpireMember();
		
		for (UUID playerId : empireMember.getAllMemberIds()) {
			final PlayerEmpire playerEmpire = playerEmpireMemory.getPlayerEmpire(playerId);
			final Empire empire = playerEmpire.getEmpire();
			
			if (empire == null || !empire.equals(this)) {
				empireMember.kickMember(playerId);
			}
		}
		
		if (empireMember.getLeaderId() == null) {
			disband();
		}
	}
}
