package net.ardeus.townsman;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.CommandTreeMemory;
import net.ardeus.townsman.empire.EmpireMemory;
import net.ardeus.townsman.manager.game.CommandTreeManager;
import net.ardeus.townsman.manager.game.EmpireManager;
import net.ardeus.townsman.manager.game.GameManager;
import net.ardeus.townsman.manager.game.TabCompleterTreeManager;
import net.ardeus.townsman.tabcompleter.TabCompleterTreeMemory;

public final class TownsmanGameMemory extends GameManager {
	
	private final CommandTreeManager commandTreeManager;
	private final TabCompleterTreeManager tabCompleterTreeManager;
	private final EmpireManager empireManager;

	protected TownsmanGameMemory(Townsman plugin) {
		super(plugin);
		
		this.commandTreeManager = CommandTreeMemory.getInstance();
		this.tabCompleterTreeManager = TabCompleterTreeMemory.getInstance();
		this.empireManager = EmpireMemory.getInstance();
	}
	
	@Override
	public final CommandTreeManager getCommandTreeManager() {
		return this.commandTreeManager;
	}
	
	@Override
	public final TabCompleterTreeManager getTabCompleterTreeManager() {
		return this.tabCompleterTreeManager;
	}
	
	@Override
	public final EmpireManager getEmpireManager() {
		return this.empireManager;
	}
}
