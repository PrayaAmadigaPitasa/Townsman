package net.ardeus.townsman.command.empire;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;

import api.praya.agarthalib.builder.support.SupportVault;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.Command;
import net.ardeus.townsman.command.CommandArgument;
import net.ardeus.townsman.empire.Empire;
import net.ardeus.townsman.language.Language;
import net.ardeus.townsman.manager.player.PlayerEmpireManager;
import net.ardeus.townsman.manager.player.PlayerManager;
import net.ardeus.townsman.player.PlayerEmpire;

public final class CommandEmpireCreate extends CommandArgument {

	private static final Command COMMAND = Command.EMPIRE_CREATE;
	
	protected CommandEmpireCreate(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_EMPIRE_CREATE.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerEmpireManager playerEmpireManager = playerManager.getPlayerEmpireManager();
		final SupportVault supportVault = supportManagerAPI.getSupportVault();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (args.length < 1) {
			final String tooltip = Language.TOOLTIP_EMPIRE_CREATE.getText(sender);
			final MessageBuild message = Language.ARGUMENT_EMPIRE_CREATE.getMessage(sender);
			
			message.sendMessage(sender, "tooltip", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = (Player) sender;
			final PlayerEmpire playerEmpire = playerEmpireManager.getPlayerEmpire(player);
			final String name = args[0];
			final double balance = supportVault != null ? supportVault.getBalance(player) : 0;
			final double cost = supportVault != null ? 1 : 0; //TODO : Cost Config
			
			//TODO : Check special character
			
			if (playerEmpire.hasEmpire()) {
				final MessageBuild message = Language.ARGUMENT_HAS_JOIN_EMPIRE.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else if (balance < cost) {
				final MessageBuild message = Language.ARGUMENT_LACK_MONEY.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final Empire empire = new Empire(name, player);
				final boolean create = empire.register();
				
				if (!create) {
					final MessageBuild message = Language.EMPIRE_CREATE_FAILED.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
					return;
				}
			}
		}
	}
}
