package net.ardeus.townsman.command.empire;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.CommandTree;

public final class CommandEmpire extends CommandTree {

	private static final String COMMAND = "Empire";
	private static final String DEFAULT_ARGUMENT = "Info";
	
	private CommandEmpire(Townsman plugin) {
		super(COMMAND, DEFAULT_ARGUMENT);
		
	}
	
	private static class CommandDreamFishSingleton {
		private static final CommandEmpire INSTANCE;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			INSTANCE = new CommandEmpire(plugin);
		}
	}
	
	public static final CommandEmpire getInstance() {
		return CommandDreamFishSingleton.INSTANCE;
	}
}