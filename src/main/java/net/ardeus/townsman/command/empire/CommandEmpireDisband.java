package net.ardeus.townsman.command.empire;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.Command;
import net.ardeus.townsman.command.CommandArgument;
import net.ardeus.townsman.empire.Empire;
import net.ardeus.townsman.empire.EmpireMemberRole;
import net.ardeus.townsman.language.Language;
import net.ardeus.townsman.manager.player.PlayerEmpireManager;
import net.ardeus.townsman.manager.player.PlayerManager;
import net.ardeus.townsman.player.PlayerEmpire;

public final class CommandEmpireDisband extends CommandArgument {

	private static final Command COMMAND = Command.EMPIRE_DISBAND;
	
	protected CommandEmpireDisband(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_EMPIRE_DISBAND.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerEmpireManager playerEmpireManager = playerManager.getPlayerEmpireManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = (Player) sender;
			final PlayerEmpire playerEmpire = playerEmpireManager.getPlayerEmpire(player);
			
			if (!playerEmpire.hasEmpire()) {
				final MessageBuild message = Language.ARGUMENT_NOT_JOIN_EMPIRE.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final EmpireMemberRole role = playerEmpire.getRole();
				
				if (!role.equals(EmpireMemberRole.LEADER)) {
					final MessageBuild message = Language.ARGUMENT_NOT_LEADER.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final Empire empire = playerEmpire.getEmpire();
					final boolean disband = empire.disband();
					
					if (!disband) {
						final MessageBuild message = Language.EMPIRE_DISBAND_FAILED.getMessage(sender);
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						return;
					}
				}
			}
		}
	}
}
