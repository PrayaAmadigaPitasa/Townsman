package net.ardeus.townsman.command.townsman;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.TownsmanConfig;
import net.ardeus.townsman.command.Command;
import net.ardeus.townsman.command.CommandArgument;
import net.ardeus.townsman.command.CommandConfig;
import net.ardeus.townsman.handler.Handler;
import net.ardeus.townsman.language.Language;
import net.ardeus.townsman.language.LanguageConfig;
import net.ardeus.townsman.placeholder.PlaceholderConfig;

public final class CommandTownsmanReload extends CommandArgument {

	private static final Command COMMAND = Command.TOWNSMAN_RELOAD;
	
	protected CommandTownsmanReload(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_TOWNSMAN_RELOAD.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
		final TownsmanConfig mainConfig = plugin.getMainConfig();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final MessageBuild message = Language.COMMAND_TOWNSMAN_RELOAD_SUCCESS.getMessage(sender);
			final PlaceholderConfig placeholderConfig = Handler.getHandler(PlaceholderConfig.class);
			final LanguageConfig languageConfig = Handler.getHandler(LanguageConfig.class);
			final CommandConfig commandConfig = Handler.getHandler(CommandConfig.class);
			
			mainConfig.setup();
			placeholderConfig.setup();
			languageConfig.setup();
			commandConfig.setup();
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
			return;
		}
	}
}
