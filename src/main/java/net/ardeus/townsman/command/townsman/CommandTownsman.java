package net.ardeus.townsman.command.townsman;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.CommandArgument;
import net.ardeus.townsman.command.CommandTree;

public final class CommandTownsman extends CommandTree {

	private static final String COMMAND = "Townsman";
	private static final String DEFAULT_ARGUMENT = "Help";
	
	private CommandTownsman(Townsman plugin) {
		super(COMMAND, DEFAULT_ARGUMENT);
		
		final CommandArgument commandArgumentAbout = new CommandTownsmanAbout(plugin);
		final CommandArgument commandArgumentHelp = new CommandTownsmanHelp(plugin);
		final CommandArgument commandArgumentReload = new CommandTownsmanReload(plugin);
		
		register(commandArgumentAbout);
		register(commandArgumentHelp);
		register(commandArgumentReload);
	}
	
	private static class CommandDreamFishSingleton {
		private static final CommandTownsman INSTANCE;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			INSTANCE = new CommandTownsman(plugin);
		}
	}
	
	public static final CommandTownsman getInstance() {
		return CommandDreamFishSingleton.INSTANCE;
	}
}