package net.ardeus.townsman.command;

import java.util.List;

import org.bukkit.command.CommandSender;

import com.praya.agarthalib.utility.SenderUtil;

import core.praya.agarthalib.builder.command.CommandBuild;

public enum Command {

	TOWNSMAN_HELP,
	TOWNSMAN_ABOUT,
	TOWNSMAN_RELOAD,
	EMPIRE_CREATE,
	EMPIRE_DISBAND;
	
	public final CommandBuild getCommandBuild() {
		final CommandMemory commandMemory = CommandMemory.getInstance();
		final CommandBuild commandBuild = commandMemory.getCommandBuild(this.toString());
		
		return commandBuild;
	}
	
	public final String getMain() {
		final CommandBuild commandBuild = getCommandBuild();
		
		return commandBuild != null ? commandBuild.getMain() : null;
	}
	
	public final String getPermission() {
		final CommandBuild commandBuild = getCommandBuild();
		
		return commandBuild != null ? commandBuild.getPermission() : null;
	}
	
	public final List<String> getAliases() {
		final CommandBuild commandBuild = getCommandBuild();
		
		return commandBuild != null ? commandBuild.getAliases() : null;
	}
	
	public final boolean checkPermission(CommandSender sender) {
		final CommandBuild commandBuild = getCommandBuild();
		
		if (commandBuild != null) {
			final String permission = commandBuild.getPermission();
			
			return SenderUtil.hasPermission(sender, permission);
		} else {
			return false;
		}
	}
	
	public final boolean checkCommand(String arg) {
		final CommandBuild commandBuild = getCommandBuild();
		
		if (commandBuild != null) {
			final String main = commandBuild.getMain();
			
			if (main.equalsIgnoreCase(arg)) {
				return true;
			} else {
				for (String aliases : commandBuild.getAliases()) {
					if (aliases.equalsIgnoreCase(arg)) {
						return true;
					}
				}
			}
		} 
		
		return false;
	}
}
