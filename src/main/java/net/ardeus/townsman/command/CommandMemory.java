package net.ardeus.townsman.command;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.builder.command.CommandBuild;
import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.CommandConfig;
import net.ardeus.townsman.manager.plugin.CommandManager;

public final class CommandMemory extends CommandManager {

	private final CommandConfig commandConfig;
	
	private CommandMemory(Townsman plugin) {
		super(plugin);
		
		this.commandConfig = new CommandConfig(plugin);
	};
	
	private static class CommandMemoryHelper {
		private static final CommandMemory instance;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			instance = new CommandMemory(plugin);
		}
	}
	
	public static final CommandMemory getInstance() {
		return CommandMemoryHelper.instance;
	}
	
	public final CommandConfig getCommandConfig() {
		return this.commandConfig;
	}
	
	@Override
	public final Collection<String> getCommandIds() {
		return getCommandIds(true);
	}
	
	protected final Collection<String> getCommandIds(boolean clone) {
		final Collection<String> commandIds = getCommandConfig().mapCommandBuild.keySet();
		
		return clone ? new ArrayList<String>(commandIds) : commandIds;
	}
	
	@Override
	public final Collection<CommandBuild> getAllCommandBuild() {
		return getAllCommandBuild(true);
	}
	
	protected final Collection<CommandBuild> getAllCommandBuild(boolean clone) {
		final Collection<CommandBuild> allCommandBuild = getCommandConfig().mapCommandBuild.values();
		
		return clone ? new ArrayList<CommandBuild>(allCommandBuild) : allCommandBuild;
	}
	
	@Override
	public final CommandBuild getCommandBuild(String id) {
		if (id != null) {
			for (String key : getCommandIds(false)) {
				if (key.equalsIgnoreCase(id)) {
					return getCommandConfig().mapCommandBuild.get(key);
				}
			}
		}
		
		return null;
	}
}
