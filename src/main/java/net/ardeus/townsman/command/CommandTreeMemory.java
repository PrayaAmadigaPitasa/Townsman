package net.ardeus.townsman.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.empire.CommandEmpire;
import net.ardeus.townsman.command.townsman.CommandTownsman;
import net.ardeus.townsman.manager.game.CommandTreeManager;

public final class CommandTreeMemory extends CommandTreeManager {

	private final HashMap<String, CommandTree> mapCommandTree = new HashMap<String, CommandTree>();
	
	private CommandTreeMemory(Townsman plugin) {
		super(plugin);
		
		final CommandTree commandTownsman = CommandTownsman.getInstance();
		final CommandTree commandEmpire = CommandEmpire.getInstance();
		
		register(commandTownsman);
		register(commandEmpire);
	}
	
	private static class CommandTreeMemorySingleton {
		private static final CommandTreeMemory instance;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			
			instance = new CommandTreeMemory(plugin);
		}
	}
	
	public static final CommandTreeMemory getInstance() {
		return CommandTreeMemorySingleton.instance;
	}
	
	@Override
	public final Collection<String> getCommands() {
		return getCommands(true);
	}
	
	public final Collection<String> getCommands(boolean clone) {
		final Collection<String> commands = this.mapCommandTree.keySet();
		
		return clone ? new ArrayList<String>(commands) : commands;
	}
	
	@Override
	public final Collection<CommandTree> getAllCommandTree() {
		return getAllCommandTree(true);
	}
	
	protected final Collection<CommandTree> getAllCommandTree(boolean clone) {
		final Collection<CommandTree> allCommandTree = this.mapCommandTree.values();
		
		return clone ? new ArrayList<CommandTree>(allCommandTree) : allCommandTree;
	}
	
	@Override
	public final CommandTree getCommandTree(String command) {
		if (command != null) {
			for (String key : getCommands(false)) {
				if (key.equalsIgnoreCase(command)) {
					return this.mapCommandTree.get(key);
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(CommandTree commandTree) {
		if (commandTree != null && !getAllCommandTree(false).contains(commandTree)) {
			final String command = commandTree.getCommand();
			final PluginCommand pluginCommand = Bukkit.getPluginCommand(command);
			
			if (pluginCommand != null) {
			
				this.mapCommandTree.put(command, commandTree);
				
				pluginCommand.setExecutor(commandTree);
				return true;
			}
		}
		
		return false;
	}
}
