package net.ardeus.townsman.structure;

import java.util.List;

public abstract class Structure {

	private final String id;
	private final List<StructureProperties> listProperties;
	
	public Structure(String id, List<StructureProperties> listProperties) {
		if (id == null || listProperties == null || listProperties.isEmpty()) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.listProperties = listProperties;
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final int getMaxGrade() {
		return this.listProperties.size();
	}
	
	public final StructureProperties getProperties(int grade) {
		return grade < getMaxGrade() ? this.listProperties.get(grade) : null;
	}
}
