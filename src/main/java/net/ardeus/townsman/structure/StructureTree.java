package net.ardeus.townsman.structure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.plugin.Plugin;

public abstract class StructureTree {

	private final Plugin plugin;
	private final String id;
	private final List<String> aliases;
	private final HashMap<String, Structure> mapStructure;
	
	protected StructureTree(Plugin plugin, String id, List<String> aliases, HashMap<String, Structure> mapStructure) {
		if (plugin == null || id == null || mapStructure == null) {
			throw new IllegalArgumentException();
		} else {
			this.plugin = plugin;
			this.id = id;
			this.aliases = aliases != null ? aliases : new ArrayList<>();
			this.mapStructure = mapStructure;
		}
	}
	
	public final Plugin getPlugin() {
		return this.plugin;
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final List<String> getAliases() {
		return getAliases(true);
	}
	
	protected final List<String> getAliases(boolean clone) {
		final List<String> aliases = this.aliases;
		
		return clone ? new ArrayList<String>(aliases) : aliases;
	}
	
	public final Collection<String> getAllStructureIds() {
		return getAllStructureIds(true);
	}
	
	protected final Collection<String> getAllStructureIds(boolean clone) {
		final Collection<String> allStructureIds = this.mapStructure.keySet();
		
		return clone ? new ArrayList<String>(allStructureIds) : allStructureIds;
	}
	
	public final Collection<Structure> getAllStructures() {
		return getAllStructures(true);
	}
	
	protected final Collection<Structure> getAllStructures(boolean clone) {
		final Collection<Structure> allStructures = this.mapStructure.values();
		
		return clone ? new ArrayList<Structure>(allStructures) : allStructures;
	}
}
