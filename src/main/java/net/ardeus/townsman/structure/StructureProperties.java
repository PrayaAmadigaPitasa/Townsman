package net.ardeus.townsman.structure;

import java.io.File;

public class StructureProperties {

	private final File file;
	private final int population;
	private final double cost;
	private final double upKeep;
	private final double point;
	
	public StructureProperties(File file) {
		this(file, 0);
	}
	
	public StructureProperties(File file, int population) {
		this(file, population, 0, 0);
	}
	
	public StructureProperties(File file, int population, double cost, double upKeep) {
		this(file, population, cost, upKeep, 0);
	}
	
	public StructureProperties(File file, int population, double cost, double upKeep, double point) {
		if (file == null) {
			throw new IllegalArgumentException();
		} else {
			this.file = file;
			this.population = population;
			this.cost = cost;
			this.upKeep = upKeep;
			this.point = point;
		}
	}
	
	public final File getFile() {
		return this.file;
	}
	
	public final int getPopulation() {
		return this.population;
	}
	
	public final double getCost() {
		return this.cost;
	}
	
	public final double getUpKeep() {
		return this.upKeep;
	}
	
	public final double getPoint() {
		return this.point;
	}
}
