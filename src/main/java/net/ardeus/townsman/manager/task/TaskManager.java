package net.ardeus.townsman.manager.task;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.handler.HandlerManager;

public abstract class TaskManager extends HandlerManager {
	
	protected TaskManager(Townsman plugin) {
		super(plugin);
	}
}
