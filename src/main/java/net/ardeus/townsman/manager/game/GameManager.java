package net.ardeus.townsman.manager.game;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.handler.HandlerManager;

public abstract class GameManager extends HandlerManager {

	protected GameManager(Townsman plugin) {
		super(plugin);
	}
	
	public abstract CommandTreeManager getCommandTreeManager();
	public abstract TabCompleterTreeManager getTabCompleterTreeManager();
	public abstract EmpireManager getEmpireManager();
}
