package net.ardeus.townsman.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.CommandArgument;
import net.ardeus.townsman.command.CommandTree;
import net.ardeus.townsman.handler.HandlerManager;

public abstract class CommandTreeManager extends HandlerManager {
	
	protected CommandTreeManager(Townsman plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getCommands();
	public abstract Collection<CommandTree> getAllCommandTree();
	public abstract CommandTree getCommandTree(String command);
	
	public final List<CommandArgument> getAllCommandArgument() {
		final List<CommandArgument> allCommandArgument = new ArrayList<CommandArgument>();
		
		for (CommandTree commandTree : getAllCommandTree()) {
			for (CommandArgument commandArgument : commandTree.getAllCommandArgument()) {
				allCommandArgument.add(commandArgument);
			}
		}
		
		return allCommandArgument;
	}
}
