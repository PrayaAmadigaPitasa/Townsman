package net.ardeus.townsman.manager.game;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.empire.Empire;
import net.ardeus.townsman.handler.HandlerManager;

public abstract class EmpireManager extends HandlerManager {

	protected EmpireManager(Townsman plugin) {
		super(plugin);
	}

	public abstract Collection<UUID> getEmpireIds();
	public abstract Collection<Empire> getAllEmpire();
	public abstract Empire getEmpire(UUID empireId);
	public abstract Empire getEmpire(String empireName);
	public abstract Empire getPlayerEmpire(UUID playerId);
	
	public final boolean isExists(UUID empireId) {
		return getEmpire(empireId) != null;
	}
	
	public final boolean isExists(String empireName) {
		return getEmpire(empireName) != null;
	}
	
	public final Empire getPlayerEmpire(OfflinePlayer player) {
		return player != null ? getPlayerEmpire(player.getUniqueId()) : null;
	}
	
	public final boolean isPlayerJoinEmpire(OfflinePlayer player) {
		return getPlayerEmpire(player) != null;
	}
	
	public final boolean isPlayerJoinEmpire(UUID playerId) {
		return getPlayerEmpire(playerId) != null;
	}
}
