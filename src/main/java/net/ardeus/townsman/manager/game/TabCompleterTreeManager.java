package net.ardeus.townsman.manager.game;

import java.util.Collection;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.handler.HandlerManager;
import net.ardeus.townsman.tabcompleter.TabCompleterTree;

public abstract class TabCompleterTreeManager extends HandlerManager {
	
	protected TabCompleterTreeManager(Townsman plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getCommands();
	public abstract Collection<TabCompleterTree> getAllTabCompleterTree();
	public abstract TabCompleterTree getTabCompleterTree(String command);
}
