package net.ardeus.townsman.manager.plugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.handler.HandlerManager;
import net.ardeus.townsman.metrics.service.BStats;

public abstract class MetricsManager extends HandlerManager {
	
	protected MetricsManager(Townsman plugin) {
		super(plugin);
	}
	
	public abstract BStats getMetricsBStats();
}
