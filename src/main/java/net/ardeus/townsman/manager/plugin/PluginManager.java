package net.ardeus.townsman.manager.plugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.handler.HandlerManager;

public abstract class PluginManager extends HandlerManager {
	
	protected PluginManager(Townsman plugin) {
		super(plugin);
	}
	
	public abstract LanguageManager getLanguageManager();
	public abstract PlaceholderManager getPlaceholderManager();
	public abstract PluginPropertiesManager getPluginPropertiesManager();
	public abstract MetricsManager getMetricsManager();
	public abstract CommandManager getCommandManager();
}
