package net.ardeus.townsman.manager.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.handler.HandlerManager;
import net.ardeus.townsman.player.PlayerEmpire;

public abstract class PlayerEmpireManager extends HandlerManager {

	protected PlayerEmpireManager(Townsman plugin) {
		super(plugin);
	}

	public abstract PlayerEmpire getPlayerEmpire(UUID playerId);
	
	public final PlayerEmpire getPlayerEmpire(OfflinePlayer player) {
		return player != null ? getPlayerEmpire(player.getUniqueId()) : null;
	}
}