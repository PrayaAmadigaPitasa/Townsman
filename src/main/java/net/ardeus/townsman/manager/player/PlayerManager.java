package net.ardeus.townsman.manager.player;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.handler.HandlerManager;

public abstract class PlayerManager extends HandlerManager {
	
	protected PlayerManager(Townsman plugin) {
		super(plugin);
	}
	
	public abstract PlayerEmpireManager getPlayerEmpireManager();
}