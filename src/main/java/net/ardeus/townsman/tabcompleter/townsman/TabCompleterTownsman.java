package net.ardeus.townsman.tabcompleter.townsman;

import net.ardeus.townsman.tabcompleter.TabCompleterArgument;
import net.ardeus.townsman.tabcompleter.TabCompleterTree;

public final class TabCompleterTownsman extends TabCompleterTree {

	private static final String COMMAND = "Townsman";
	
	private TabCompleterTownsman() {
		super(COMMAND);
		
		final TabCompleterArgument tabCompleterArgumentHelp = TabCompleterTownsmanHelp.getInstance();
		
		register(tabCompleterArgumentHelp);
	}
	
	private static class CommandHerbalismSingleton {
		private static final TabCompleterTownsman instance = new TabCompleterTownsman();
	}
	
	public static final TabCompleterTownsman getInstance() {
		return CommandHerbalismSingleton.instance;
	}
}