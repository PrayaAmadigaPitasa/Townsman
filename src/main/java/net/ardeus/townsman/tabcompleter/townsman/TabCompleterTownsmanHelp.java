package net.ardeus.townsman.tabcompleter.townsman;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.townsman.Townsman;
import net.ardeus.townsman.command.Command;
import net.ardeus.townsman.tabcompleter.TabCompleterArgument;

public final class TabCompleterTownsmanHelp extends TabCompleterArgument {

	private static final Command COMMAND = Command.TOWNSMAN_HELP;
	
	private TabCompleterTownsmanHelp(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterHerbalismHelpSingleton {
		private static final TabCompleterTownsmanHelp instance;
		
		static {
			final Townsman plugin = JavaPlugin.getPlugin(Townsman.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterTownsmanHelp(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterTownsmanHelp getInstance() {
		return TabCompleterHerbalismHelpSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("[<page>]");
		}
		
		return tabList;
	}
}
